//
//  ViewController.swift
//  CoreDataFacil
//
//  Created by Laboratorio FIS on 23/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var adressTextField: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var singlePerson: Person?
    var arrayAllPerson:[Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func savePerson(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.name = nameTextField.text ?? ""
        person.adress = adressTextField.text ?? ""
        person.phone = phoneTextfield.text ?? ""
        
        do {
            try manageObjectContext.save()
            clearfields()
        } catch {
            print("Error")
        }
    }
    
    func clearfields(){
        nameTextField.text = ""
        adressTextField.text = ""
        phoneTextfield.text = ""
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == "" {
            findAll()
            return
        }
        
        findone()
    }
    
    func findAll() {
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results {
                
                let person = result as! Person
                arrayAllPerson.append(person)
                //print("Name: \(person.name ?? "") ", terminator: "")
                //print("Adress: \(person.adress ?? "") ", terminator: "")
                //print("Phone: \(person.phone ?? "") ", terminator: "")
                
                //print()
                //print()
            }
            
            performSegue(withIdentifier: "findAllSegue", sender: self)
            
        }catch{
            print("Error finding people")
        }
    }
    
    func findone() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        
        do {
            
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let match = results[0] as! Person
                singlePerson = match
                performSegue(withIdentifier: "findOneSegue", sender: self)
               // adressTextField.text = match.adress
               // nameTextField.text = match.name
               // phoneTextfield.text = match.phone
            } else {
                adressTextField.text = "n/a"
                nameTextField.text = "n/a"
                phoneTextfield.text = "n/a"
            }
            
        }catch{
            print("Error finding one")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "findOneSegue" {
            let destination = segue.destination as! FindOneViewController
            destination.person = singlePerson
        }
        if segue.identifier == "findAllSegue" {
            let destination = segue.destination as! FindAllViewController
            destination.personArray = arrayAllPerson
        }
        
    }
}

