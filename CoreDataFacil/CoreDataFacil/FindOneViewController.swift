//
//  FindOneViewController.swift
//  CoreDataFacil
//
//  Created by Laboratorio FIS on 31/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class FindOneViewController: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = person?.name
        addressLabel.text = person?.adress!
        phoneLabel.text = person?.phone
    }

    
    @IBAction func deletButtonPressed(_ sender: Any) {
        
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        do {
            try manageObjectContext.save()
        } catch {
            print("Error deleting object")
        }
        
    }
    
}
