//
//  TableViewCell.swift
//  CoreDataFacil
//
//  Created by Laboratorio FIS on 31/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillCell(person: Person) {
        nameLabel.text = person.name
    }
}
