//
//  FindAllViewController.swift
//  CoreDataFacil
//
//  Created by Laboratorio FIS on 31/1/18.
//  Copyright © 2018 Laboratorio FIS. All rights reserved.
//

import UIKit

class FindAllViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var PersonIndex = 0
    var personArray:[Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personNameCell") as! TableViewCell
        cell.fillCell(person: personArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return "sección 1"
        default:
            return "sección 2"
        }
    }
}
